#!/bin/sh

while ! nc -z 'mongo.hlebnitca' '27017';
	do sleep 1;
	echo "await MongoDB";
done

while ! nc -z 'rabbit.hlebnitca' '5672';
	do sleep 1;
	echo "await RabbitMQ";
done

cd /app
mkdir ./runtime/logs
chmod 0777 -R ./runtime
chmod 0777 -R ./web/assets

composer install

php yii migrate --interactive=0
php yii fill-test-data

service supervisor start
apache2-foreground
