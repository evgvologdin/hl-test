#!/bin/sh

echo "Create user ${MONGO_APP_USERNAME} on DB ${MONGO_APP_DATABASE}"

mongo -u $MONGO_INITDB_ROOT_USERNAME -p $MONGO_INITDB_ROOT_PASSWORD --host localhost --eval "
    db.getSiblingDB('${MONGO_APP_DATABASE}').createUser({
        user: '${MONGO_APP_USERNAME}',
        pwd: '${MONGO_APP_PASSWORD}',
        roles:[{role:'readWrite',db:'${MONGO_APP_DATABASE}'}]
    });
"
