<?php

define('QUEUE_DEFAULT_PRODUCER', 'hlebnitca.default.producer');
define('QUEUE_EXCHANGE_DIRECT', 'hlebnitca.exchange.direct');

return [
    'class' => \app\components\rabbitmq\Configuration::class,
    'connections' => [
        [
            'host'     => 'rabbit.hlebnitca',
            'port'     => '5672',
            'user'     => 'rabbitmq',
            'password' => 'rabbitmq',
        ]
    ],
    'models' => [
        \app\modules\check\models\async\Create::class
    ]
];
