<?php

return [
    'class' => yii\web\UrlManager::class,
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        [
            'class' => \yii\rest\UrlRule::class,
            'controller' => [
                'cashbox' => 'cashbox/default'
            ],
            'patterns' => [
                'GET' => 'view',
                '' => 'options',
            ]
        ],
        [
            'class' => \yii\rest\UrlRule::class,
            'controller' => [
                'check' => 'check/default'
            ],
            'patterns' => [
                'POST' => 'create',
                '' => 'options',
            ]
        ],
        [
            'class' => \yii\rest\UrlRule::class,
            'controller' => [
                'statistics/summary' => 'statistics/default'
            ],
            'patterns' => [
                'POST' => 'summary-view',
                '' => 'options',
            ]
        ],
    ]
];
