<?php

return [
    'traceLevel' => YII_DEBUG ? 10 : 0,

    'targets' => [
        [
            'class' => yii\log\FileTarget::class,
            'logVars' => [],
            'maxFileSize' => 1024 * 5,
            'maxLogFiles' => 40,
            'microtime' => true,
            'categories' => ['rabbitmq.unsaved'],
            'logFile' => '@runtime/logs/rabbitmq.unsaved.log'
        ]
    ]
];
