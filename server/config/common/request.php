<?php

return [
    'class' => yii\web\Request::class,
    'cookieValidationKey' => 'fsmllkKLl9fekf94f9oj23rd32Dr343dFfd',
    'enableCsrfValidation' => false,
    'parsers' => [
        'application/json' => \yii\web\JsonParser::class,
    ]
];
