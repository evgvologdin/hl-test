<?php

return [
    'check' => [
        'class' => \app\modules\check\Module::class,
    ],
    'cashbox' => [
        'class' => \app\modules\cashbox\Module::class,
    ],
    'statistics' => [
        'class' => \app\modules\statistics\Module::class,
    ],
    'error' => [
        'class' => \app\modules\error\Module::class,
    ],
];
