<?php

return [
    'id' => 'cli-hlebnitca',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migrate' => \yii\mongodb\console\controllers\MigrateController::class
    ],
    'timeZone' => 'UTC',
    'params' => require_once __DIR__ . '/console/params.php',
    'aliases' => require_once __DIR__ . '/console/aliases.php',
    'modules' => require_once __DIR__ . '/console/modules.php',
    'components' => [
        'mongodb' => require_once __DIR__ . '/console/mongodb.php',
        'log' => require_once __DIR__ . '/console/log.php',
        'errorHandler' => require_once __DIR__ . '/console/error-handler.php',
        'rabbitmq' => require_once __DIR__ . '/console/rabbitmq.php',
    ]
];
