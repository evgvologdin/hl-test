<?php

return [
    'id' => 'dev-hlebnitca',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'debug', 'gii'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'timeZone' => 'UTC',
    'params' => require_once __DIR__ . '/dev/params.php',
    'aliases' => require_once __DIR__ . '/dev/aliases.php',
    'modules' => require_once __DIR__ . '/dev/modules.php',
    'components' => [
        'mongodb' => require_once __DIR__ . '/dev/mongodb.php',
        'log' => require_once __DIR__ . '/dev/log.php',
        'errorHandler' => require_once __DIR__ . '/dev/error-handler.php',
        'urlManager' => require_once __DIR__ . '/dev/url-manager.php',
        'request' => require_once __DIR__ . '/dev/request.php',
        'rabbitmq' => require_once __DIR__ . '/dev/rabbitmq.php',
    ]
];
