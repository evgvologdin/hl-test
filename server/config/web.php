<?php

return [
    'id' => 'web-hlebnitca',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'sourceLanguage' => 'ru-RU',
    'timeZone' => 'UTC',
    'params' => require_once __DIR__ . '/web/params.php',
    'aliases' => require_once __DIR__ . '/web/aliases.php',
    'modules' => require_once __DIR__ . '/web/modules.php',
    'components' => [
        'mongodb' => require_once __DIR__ . '/web/mongodb.php',
        'log' => require_once __DIR__ . '/web/log.php',
        'errorHandler' => require_once __DIR__ . '/web/error-handler.php',
        'urlManager' => require_once __DIR__ . '/web/url-manager.php',
        'request' => require_once __DIR__ . '/web/request.php',
        'rabbitmq' => require_once __DIR__ . '/web/rabbitmq.php',
    ]
];
