<?php

return \yii\helpers\ArrayHelper::merge(require_once __DIR__ . '/../common/log.php', [
    'targets' => [
        [
            'class' => yii\log\FileTarget::class,
            'levels' => ['error'],
            'logVars' => [],
            'maxFileSize' => 1024 * 5,
            'maxLogFiles' => 40,
            'microtime' => true,
            'logFile' => '@runtime/logs/cli.error.log'
        ],
        [
            'class' => yii\log\FileTarget::class,
            'levels' => ['warning'],
            'logVars' => [],
            'maxFileSize' => 1024 * 5,
            'maxLogFiles' => 40,
            'microtime' => true,
            'logFile' => '@runtime/logs/cli.warning.log'
        ]
    ],
]);