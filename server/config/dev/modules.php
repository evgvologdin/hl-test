<?php

return \yii\helpers\ArrayHelper::merge(require_once __DIR__ . '/../common/modules.php', [
    'debug' => [
        'class' => yii\debug\Module::class,
        'allowedIPs' => ['*'],
        'panels' => [
            'MongoDB' => [
                'class' => \yii\mongodb\debug\MongoDbPanel::class
            ]
        ]
    ],
    'gii' => [
        'class' => yii\gii\Module::class,
        'allowedIPs' => ['*'],
        'generators' => [
            'MongoDBModel' => [
                'class' => \yii\mongodb\gii\model\Generator::class
            ]
        ],
    ]
]);
