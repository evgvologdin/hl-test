<?php

namespace app\components\formatters;

class SuccessResponseFormatter implements \JsonSerializable
{
    /**
     * @var \JsonSerializable|array|null
     */
    private $_body;

    public function __construct($body = null)
    {
        $this->_body = $body;
    }

    public function jsonSerialize()
    {
        return ['status' => 'ok', 'response' => $this->_body];
    }
}
