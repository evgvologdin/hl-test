<?php

namespace app\components\mongodb;

use Yii;
use yii\base\Behavior;
use yii\base\InvalidArgumentException;
use yii\base\ModelEvent;
use yii\mongodb\ActiveRecord;
use yii\mongodb\Connection;

class AutoIncrementBehavior extends Behavior
{
    public function events()
    {
        return array_merge(parent::events(), [
            'beforeInsert' => ActiveRecord::EVENT_BEFORE_INSERT
        ]);
    }

    public function beforeInsert(ModelEvent $event)
    {
        /** @var ActiveRecord $model */
        $model = $event->sender;

        if (!($model instanceof ActiveRecord)) {
            throw new InvalidArgumentException('Event sender must be ' . ActiveRecord::class);
        }

        $model->_id = $this->sequence($model::collectionName()[1]);
    }

    protected function sequence($name)
    {
        /** @var Connection $mongodb */
        $mongodb = Yii::$app->mongodb;
        $result  = $mongodb->getCollection("sequence")->findAndModify(
            ['_id' => $name],
            ['$inc' => ['value' => 1]],
            ['upsert' => true, 'new' => true]
        );
        return (int) $result['value'];
    }
}
