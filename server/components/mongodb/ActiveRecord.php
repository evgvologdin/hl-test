<?php
/**
 * @author Evgeniy Vologdin <muns@muns.su>
 * @since 08.07.19
 * @version 0.1
 * @copyright © Evgeniy Vologdin, 2019
 */

namespace app\components\mongodb;

/**
 * Class ActiveRecord
 * @package app\components\mongodb
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property integer $id
 */
class ActiveRecord extends \yii\mongodb\ActiveRecord
{
    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            AutoIncrementBehavior::class => [
                'class' => AutoIncrementBehavior::class
            ]
        ]);
    }

    public function attributes()
    {
        return ['_id'];
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getId()
    {
        return $this->_id;
    }
}
