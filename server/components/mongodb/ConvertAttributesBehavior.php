<?php
/**
 * @author Evgeniy Vologdin <muns@muns.su>
 * @since 11.07.19
 * @version 0.1
 * @copyright © Evgeniy Vologdin, 2019
 */

namespace app\components\mongodb;

use yii\base\Behavior;
use yii\base\Event;

class ConvertAttributesBehavior extends Behavior
{
    /**
     * @var array
     */
    public $attributes = [];

    /**
     * @var \Closure
     */
    public $onSave;

    /**
     * @var \Closure
     */
    public $onView;

    public function events()
    {
        return array_merge(parent::events(), [
            ActiveRecord::EVENT_AFTER_FIND    => 'onView',
            ActiveRecord::EVENT_AFTER_INSERT  => 'onView',
            ActiveRecord::EVENT_AFTER_UPDATE  => 'onView',
            ActiveRecord::EVENT_BEFORE_INSERT => 'onSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'onSave',
        ]);
    }

    public function onView(Event $event)
    {
        $model = $event->sender;

        if (!($model instanceof ActiveRecord)) {
            throw new InvalidArgumentException('Event sender must be ' . ActiveRecord::class);
        }

        if (!is_callable($this->onView)) {
            return;
        }

        foreach ($this->attributes as $attribute) {
            $model->{$attribute} = call_user_func_array($this->onView, [$model->{$attribute}]);
        }
    }

    public function onSave(Event $event)
    {
        $model = $event->sender;

        if (!($model instanceof ActiveRecord)) {
            throw new InvalidArgumentException('Event sender must be ' . ActiveRecord::class);
        }

        if (!is_callable($this->onSave)) {
            return;
        }

        foreach ($this->attributes as $attribute) {
            $model->{$attribute} = call_user_func_array($this->onSave, [$model->{$attribute}]);
        }
    }
}
