<?php

namespace app\components\web;

use Yii;
use yii\filters\Cors;
use yii\web\Response;

class Controller extends \yii\web\Controller
{
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        parent::init();
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            Cors::class => [
                'class' => Cors::class,
                'cors' => [
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Max-Age' => 3600,
                    'Access-Control-Expose-Headers' => ["*"],
                ],
            ],
        ]);
    }

    public function actionOptions()
    {
    }
}
