<?php

namespace app\components\rabbitmq;


use yii\base\InvalidArgumentException;

class Configuration extends \mikemadisonweb\rabbitmq\Configuration
{
    public $models = [];

    public function init()
    {
        $this->generateConfigurations($this->models);
    }

    protected function generateConfigurations(array $models)
    {
        /** @var QueueModel $model */
        foreach ($models as $model) {

            if (
                !class_exists($model) ||
                !array_search(QueueModel::class, class_parents($model))
            ) {
                throw new InvalidArgumentException('model ' . $model . ' must be ' . QueueModel::class);
            }

            if (
                !class_exists($model::getConsumer()) ||
                !array_search(QueueConsumer::class, class_parents($model::getConsumer()))
            ) {
                throw new InvalidArgumentException('consumenr ' . $model::getConsumer() . ' must be ' . QueueConsumer::class);
            }

            $this->generateQueuesConfiguration($model::getQueueName());
            $this->generateExchangesConfiguration($model::getExchangeName());
            $this->generateProducersConfiguration($model::getProducerName());
            $this->generateBindingsConfiguration($model::getQueueName(), $model::getExchangeName(), $model::getRoutingKey());
            $this->generateConsumersConfiguration($model::getQueueName(), $model::getConsumer());
        }
    }

    protected function generateQueuesConfiguration(string $name)
    {
        foreach ($this->queues as $queue) {
            if ($queue['name'] == $name) {
                return;
            }
        }
        $this->queues[] = ['name' => $name];
    }

    protected function generateProducersConfiguration(string $name)
    {
        foreach ($this->producers as $producer) {
            if ($producer['name'] == $name) {
                return;
            }
        }
        $this->producers[] = ['name' => $name];
    }

    protected function generateExchangesConfiguration(string $name)
    {
        foreach ($this->exchanges as $exchange) {
            if ($exchange['name'] == $name) {
                return;
            }
        }
        $this->exchanges[] = ['name' => $name, 'type' => 'direct'];
    }

    protected function generateBindingsConfiguration(string $queue, string $exchange, string $routingKey)
    {
        foreach ($this->bindings as &$binding) {
            if ($binding['queue'] == $queue && $binding['exchange'] = $exchange) {
                $binding['routing_keys'] = $routingKey;
                $binding['routing_keys'] = array_unique('routing_keys');
                unset($binding);
                return;
            }
            unset($binding);
        }
        $this->bindings[] = [
            'queue' => $queue,
            'exchange' => $exchange,
            'routing_keys' => [$routingKey]
        ];
    }

    protected function generateConsumersConfiguration(string $queue, string $consumer)
    {
        foreach ($this->consumers as &$config) {
            if ($config['name'] == $consumer) {
                $config['callbacks'][$queue] = $consumer;
                return;
            }
            unset($config);
        }

        $this->consumers[] = [
            'name' => $consumer::getName(),
            'callbacks' => [
                $queue => $consumer
            ],
            'qos' => [
                'prefetch_size' => 0,
                'prefetch_count' => 1,
            ],
            'idle_timeout' => 120,
            'idle_timeout_exit_code' => 0,
        ];
    }
}
