<?php

namespace app\components\rabbitmq;

use Yii;
use mikemadisonweb\rabbitmq\components\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use yii\base\InvalidCallException;

abstract class QueueConsumer implements ConsumerInterface
{
    public static function getName(): string
    {
        return str_replace(
            'app.', '', str_replace(
                '\\', '.', static::class
            )
        );
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            /** @var QueueModel $model */
            $model = new $msg->body['class'];
            $model->setAttributes($msg->body['attributes']);

            if (!$this->save($model, $msg)) {
                throw new InvalidCallException('unsaved model ' . $msg->body['class'] . ', errors: ' . json_encode($model->getErrors()));
            }
            return self::MSG_ACK;

        } catch (\Exception $e) {
            if (!$msg->delivery_info['redelivered']) {
                Yii::warning($e);
                Yii::getLogger()->flush(true);
                return self::MSG_REQUEUE;
            } else {
                Yii::error($e);
                Yii::error($msg->body, 'rabbitmq.unsaved');
                Yii::getLogger()->flush(true);
                return self::MSG_REJECT;
            }
        }


    }
}
