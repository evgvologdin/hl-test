<?php

namespace app\components\rabbitmq;

use Yii;
use mikemadisonweb\rabbitmq\components\Producer;
use yii\base\Model;

abstract class QueueModel extends Model
{
    abstract public static function getExchangeName(): string;

    abstract public static function getConsumer(): string;

    public static function getProducerName(): string
    {
        return QUEUE_DEFAULT_PRODUCER;
    }

    public static function getQueueName(): string
    {
        return str_replace(
            'app.', '', str_replace(
                '\\', '.', static::class
            )
        );
    }

    public static function getRoutingKey(): string
    {
        return static::class;
    }

    public function save($runValidation = true): bool
    {
        if ($runValidation && !$this->validate()) {
            return false;
        }

        try {
            /** @var Producer $producer */
            $producer = Yii::$app->rabbitmq->getProducer(static::getProducerName());
            $producer->publish(
                ['class' => static::class, 'attributes' => $this->getAttributes()],
                static::getExchangeName(),
                static::getRoutingKey()
            );

            return true;
        } catch (\Exception $e) {
            Yii::error($e);
            return false;
        }
    }
}
