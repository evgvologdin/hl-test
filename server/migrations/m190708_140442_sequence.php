<?php

use yii\mongodb\Migration;

/**
 * Class m190708_140442_sequence
 */
class m190708_140442_sequence extends Migration
{
    public function up()
    {
        $this->createCollection('sequence', [
            'validator' => [
                '$jsonSchema' => [
                    'bsonType' => 'object',
                    'required' => ['_id', 'value'],
                ]
            ]
        ]);
    }
    public function down()
    {
        $this->dropCollection('sequence');
    }
}
