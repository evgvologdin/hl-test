<?php

use yii\mongodb\Migration;

/**
 * Class m190708_140452_check
 */
class m190708_140452_check extends Migration
{
    public function up()
    {
        $this->createCollection('check', [
            'validator' => [
                '$jsonSchema' => [
                    'bsonType' => 'object',
                    'required' => ['_id', 'cashboxId', 'totalSum', 'dateTime'],
                    'properties' => [
                        'cashboxId' => [
                            'bsonType' => 'int'
                        ],
                        'totalSum' => [
                            'bsonType' => 'int'
                        ],
                        'dateTime' => [
                            'bsonType' => 'date'
                        ]
                    ]
                ]
            ]
        ]);

        $this->createIndex('check', 'datetime');
    }
    public function down()
    {
        $this->dropCollection('check');
    }
}
