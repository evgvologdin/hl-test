<?php

use yii\mongodb\Migration;

/**
 * Class m190708_140448_cashbox
 */
class m190708_140448_cashbox extends Migration
{
    public function up()
    {
        $this->createCollection('cashbox', [
            'validator' => [
                '$jsonSchema' => [
                    'bsonType' => 'object',
                    'required' => ['_id', 'timeZoneOffset'],
                    'properties' => [
                        'timeZoneOffset' => [
                            'bsonType' => 'int'
                        ]
                    ]
                ]
            ]
        ]);
    }
    public function down()
    {
        $this->dropCollection('cashbox');
    }
}
