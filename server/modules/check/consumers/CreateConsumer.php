<?php

namespace app\modules\check\consumers;

use app\components\rabbitmq\QueueConsumer;
use app\modules\cashbox\models\mongo\Cashbox;
use app\modules\check\models\async\Create;
use app\modules\check\models\mongo\Check;

class CreateConsumer extends QueueConsumer
{
    public static function getName(): string
    {
        return 'check.create';
    }

    protected function save(Create $model): bool
    {
        $dateTime = new \DateTimeImmutable($model->dateTime);

        if (!$model->isSafeDateTime) {
            $timeOffset = Cashbox::find()->getTimeZoneOffset($model->cashboxId);
            if ($timeOffset <> 0) {
                $timeOffset = 0 - $timeOffset;
                $dateTime = $dateTime->modify("${timeOffset} seconds");
            }

        }

        $checkModel = new Check();
        $checkModel->dateTime = $dateTime->format('Y-m-d H:i:s');
        $checkModel->cashboxId = $model->cashboxId;
        $checkModel->totalSum = $model->totalSum;

        return $checkModel->save();
    }
}
