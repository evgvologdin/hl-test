<?php

namespace app\modules\check\controllers\actions;

use Yii;
use app\components\formatters\ErrorsResponseFormatter;
use app\components\formatters\SuccessResponseFormatter;
use app\modules\check\models\async\Create;
use yii\base\Action;

class CreateAction extends Action
{
    public function run()
    {
        $model = new Create();

        if (
            $model->load(Yii::$app->request->getBodyParams(), '') &&
            $model->save()
        ) {
            return new SuccessResponseFormatter();
        } else {
            return new ErrorsResponseFormatter($model->getErrors());
        }
    }
}
