<?php

namespace app\modules\check\controllers;

use app\components\web\Controller;
use app\modules\check\actions\CreateAction;

class DefaultController extends Controller
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'create' => actions\CreateAction::class
        ]);
    }
}
