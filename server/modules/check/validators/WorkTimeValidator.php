<?php

namespace app\modules\check\validators;

use app\modules\cashbox\models\mongo\Cashbox;
use app\modules\check\models\async\Create;
use yii\base\InvalidArgumentException;
use yii\validators\Validator;

class WorkTimeValidator extends Validator
{
    /**
     * @var string
     */
    public $message = 'Добавление чеков возможно с {startTime} до {endTime} часов, текущее время {currentTime}';

    /**
     * @var integer
     */
    public $startTime = 6;

    /**
     * @var integer
     */
    public $endTime = 22;

    public function validateAttribute($model, $attribute)
    {
        if (!($model instanceof Create)) {
            throw new InvalidArgumentException('model attribute must be ' . Create::class);
        }

        $dateTime = new \DateTimeImmutable($model->{$attribute});

        if ($model->isSafeDateTime) {
            $timeOffset = Cashbox::find()->getTimeZoneOffset($model->cashboxId);

            if ($timeOffset <> 0) {
                $dateTime = $dateTime->modify("${timeOffset} second");
            }
        }

        $hours = (int) $dateTime->format('H');

        if ($hours < $this->startTime || $hours >= $this->endTime) {
            $this->addError(
                $model, $attribute, $this->message, [
                    'startTime' => $this->startTime,
                    'endTime' => $this->endTime,
                    'currentTime' => $dateTime->format("H:i:s")
                ]
            );
        }
    }
}
