<?php
namespace app\modules\check\models\async;

use app\components\rabbitmq\QueueConsumer;
use app\components\rabbitmq\QueueModel;
use app\modules\cashbox\models\mongo\Cashbox;
use app\modules\check\consumers\CreateConsumer;
use app\modules\check\validators\WorkTimeValidator;

/**
 * Class Create
 * @package app\modules\check\forms
 * @property integer $cashboxId
 * @property float $totalSum
 * @property string $dateTime
 * @property boolean $isSafeDateTime
 */
class Create extends QueueModel
{
    /**
     * @var integer
     */
    public $cashboxId;

    /**
     * @var float
     */
    public $totalSum;

    /**
     * @var string
     */
    private $_dateTime;

    /**
     * @var bool
     */
    private $_isSafeDateTime = true;

    public function setDateTime(?string $dateTime)
    {
        $this->_dateTime = $dateTime;
        $this->_isSafeDateTime = false;
    }

    public function getDateTime(): string
    {
        if (empty($this->_dateTime)) {
            $this->_dateTime = date('Y-m-d H:i:s');
        }

        return $this->_dateTime;
    }

    public function setIsSafeDateTime(bool $isSafe)
    {
        $this->_isSafeDateTime = $isSafe;
    }

    public function getIsSafeDateTime(): bool
    {
        return $this->_isSafeDateTime;
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['cashboxId', 'totalSum', 'dateTime'],
                'required'
            ],
            [
                ['cashboxId'],
                'exist', 'targetClass' => Cashbox::class, 'targetAttribute' => '_id'
            ],
            [
                ['dateTime'],
                'datetime', 'format' => 'yyyy-M-d H:m:s'
            ],
            [
                ['dateTime'],
                WorkTimeValidator::class
            ]
        ]);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['dateTime', 'isSafeDateTime']);
    }

    /**
     * @return string
     */
    static public function getExchangeName(): string
    {
        return QUEUE_EXCHANGE_DIRECT;
    }

    /**
     * @return string
     */
    static public function getConsumer(): string
    {
        return CreateConsumer::class;
    }
}
