<?php


namespace app\modules\check\models\mongo;

use MongoDB\BSON\UTCDateTime;
use app\modules\statistics\models\forms\Summary;
use yii\mongodb\ActiveQuery;

class CheckQuery extends ActiveQuery
{
    public function getSummaryStatistics(Summary $form): array
    {
        return $this->getCollection()->aggregate([
            [
                // Выборка по датам
                '$match' => [
                    'dateTime' => [
                        '$gte' => new UTCDateTime($form->startDateWithOffset->getTimestamp() * 1000),
                        '$lt'  => new UTCDateTime($form->endDateWithOffset->getTimestamp() * 1000)
                    ]
                ],
            ],
            [
                // Поля для группировки и смещение дат по таймзоне
                '$project' => [
                    'dateTime' => [
                        '$toDate' => [
                            '$dateToString' => $form->timeZone
                                ? ['date' => '$dateTime', 'timezone' => $form->timeZone]
                                : ['date' => '$dateTime']
                        ]
                    ],
                    'cashboxId' => '$cashboxId',
                    'totalSum' => '$totalSum'
                ]
            ],
            [
                // Группировка по дате и кассам
                '$group' => [
                    '_id' => [
                        'group' => [$form->getGroupBy() => '$dateTime'],
                        'cashboxId' => '$cashboxId'
                    ],
                    'minDateTime' => ['$first' => '$dateTime'],
                    'maxDateTime' => ['$last' => '$dateTime'],
                    'totalSum' => ['$sum' => '$totalSum']
                ],
            ],
            [
                // Группировка по дате
                '$group' => [
                    '_id' => '$_id.group',
                    'minDateTime' => ['$first' => '$minDateTime'],
                    'maxDateTime' => ['$last' => '$maxDateTime'],
                    'cashboxes' => [
                        '$push' => [
                            'cashboxId' => '$_id.cashboxId',
                            'totalSum' => '$totalSum'
                        ]
                    ]
                ]
            ],
            [
                '$sort' => ['_id' => 1]
            ]
        ]);
    }
}
