<?php

namespace app\modules\check\models\mongo;

use app\components\mongodb\ActiveRecord;
use app\components\mongodb\ConvertAttributesBehavior;
use app\modules\cashbox\models\mongo\CashboxQuery;
use MongoDB\BSON\UTCDateTime;
use yii\base\Action;
use yii\behaviors\AttributesBehavior;
use yii\behaviors\AttributeTypecastBehavior;

/**
 * Class Check
 * @package app\modules\cashbox\models
 * @property integer $cashboxId
 * @property UTCDateTime|string $dateTime
 * @property float $totalSum
 */
class Check extends ActiveRecord
{
    public static function collectionName()
    {
        return ['hlebnitca', 'check'];
    }

    public function behaviors()
    {
        return array_merge(parent::behaviors(), [
            'dateTimeConvert' => [
                'class' => ConvertAttributesBehavior::class,
                'attributes' => ['dateTime'],
                'onView' => function ($value) {
                    return $value instanceof UTCDateTime
                        ? $value->toDateTime()->format('Y-m-d H:i:s')
                        : $value;
                },
                'onSave' => function ($value) {
                    return $value instanceof UTCDateTime
                        ? $value
                        : new UTCDateTime(strtotime($value) * 1000);
                }
            ],
            'totalSumConvert' => [
                'class' => ConvertAttributesBehavior::class,
                'attributes' => ['totalSum'],
                'onView' => function ($value) {
                    return $value / 100;
                },
                'onSave' => function ($value) {
                    return (int) ($value * 100);
                }
            ]
        ]);
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['cashboxId', 'dateTime', 'totalSum']);
    }

    public function rules()
    {
        return [
            [['cashboxId', 'dateTime', 'totalSum'], 'safe'],
        ];
    }

    public static function find()
    {
        return new CheckQuery(get_called_class());
    }
}
