<?php

namespace app\modules\statistics\formatters;

use Yii;
use app\modules\statistics\models\forms\Summary;
use yii\base\InvalidArgumentException;

class SummaryFormatter implements \JsonSerializable
{
    /**
     * @var Summary
     */
    private $_form;

    /**
     * @var array
     */
    private $_statistics;

    public function __construct(Summary $form, array $statistics)
    {
        $this->_form = $form;
        $this->_statistics = $statistics;
    }

    public function jsonSerialize()
    {
        return array_map(function (array $item) {

            /** @var \DateTimeInterface $dateTime */
            $dateTime = $item['minDateTime']->toDateTime();

            switch ($this->_form->groupBy) {

                case Summary::GROUP_BY_YEARS:
                    $startDate = new \DateTimeImmutable($dateTime->format('Y-01-01 00:00:00'));
                    $endDate = $startDate->modify('+1 year');
                    $name = Yii::$app->formatter->asDatetime($startDate, 'yyyy');
                    break;
                case Summary::GROUP_BY_MONTHS:
                    $startDate = new \DateTimeImmutable($dateTime->format('Y-m-01 00:00:00'));
                    $endDate = $startDate->modify('+1 month');
                    $name = Yii::$app->formatter->asDatetime($startDate, 'LLLL yyyy');
                    break;
                case Summary::GROUP_BY_DAYS:
                    $startDate = new \DateTimeImmutable($dateTime->format('Y-m-d 00:00:00'));
                    $endDate = $startDate->modify('+1 day');
                    $name = Yii::$app->formatter->asDatetime($startDate, 'dd.MM.yyyy (eeeeee)');
                    break;
                case Summary::GROUP_BY_HOURS:
                    $startDate = new \DateTimeImmutable($dateTime->format('Y-m-d H:00:00'));
                    $endDate = $startDate->modify('+1 hour');
                    $name = Yii::$app->formatter->asDatetime($startDate, 'dd.MM.yyyy (eeeeee) HH:00');
                    break;
                default:
                    throw new InvalidArgumentException("unsupported group type {$this->_form->groupBy}");
            }

            return [
                'name' => $name,
                'query' => [
                    'startDate' => $startDate->format('Y-m-d H:i:s'),
                    'endDate' => $endDate->format('Y-m-d H:i:s'),
                    'timeZoneOffset' => $this->_form->timeZoneOffset
                ],
                'cashboxes' => array_map(function (array $value) {
                    $value['totalSum'] = $value['totalSum'] / 100;
                    return $value;
                }, $item['cashboxes'])
            ];

        }, $this->_statistics);
    }
}
