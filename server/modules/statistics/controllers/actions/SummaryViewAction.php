<?php

namespace app\modules\statistics\controllers\actions;

use Yii;
use yii\base\Action;
use app\components\formatters\ErrorsResponseFormatter;
use app\components\formatters\SuccessResponseFormatter;
use app\modules\check\models\mongo\Check;
use app\modules\statistics\models\forms\Summary;
use app\modules\statistics\formatters\SummaryFormatter;

class SummaryViewAction extends Action
{
    public function run()
    {
        $form = new Summary();
        $form->load(Yii::$app->request->getBodyParams(), '');

        if ($form->validate()) {

            $statistics = Check::find()->getSummaryStatistics($form);

            return new SuccessResponseFormatter(
                new SummaryFormatter($form, $statistics)
            );
        } else {
            return new ErrorsResponseFormatter($form->getErrors());
        }
    }
}
