<?php

namespace app\modules\statistics\controllers;

use app\components\web\Controller;

class DefaultController extends Controller
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'summary-view' => actions\SummaryViewAction::class
        ]);
    }
}
