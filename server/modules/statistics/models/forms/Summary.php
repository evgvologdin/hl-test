<?php

namespace app\modules\statistics\models\forms;


use yii\base\Model;

/**
 * Class Summary
 * @package app\modules\statistics\models\forms
 * @property string|null $timeZone
 * @property \DateTimeInterface $startDateWithOffset
 * @property \DateTimeInterface $endDateWithOffset
 * @property string $groupBy
 */
class Summary extends Model
{
    const GROUP_BY_YEARS  = '$year';
    const GROUP_BY_MONTHS = '$month';
    const GROUP_BY_DAYS   = '$dayOfMonth';
    const GROUP_BY_HOURS  = '$hour';

    /**
     * @var string
     */
    public $startDate;

    /**
     * @var string
     */
    public $endDate;

    /**
     * @var int
     */
    public $timeZoneOffset = 3 * 60 * 60;

    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['timeZoneOffset', 'startDateWithOffset', 'endDateWithOffset', 'groupBy'],
                'required'
            ],
            [
                ['startDate', 'endDate'],
                'datetime', 'format' => 'yyyy-M-d H:m:s'
            ],
            [
                ['startDate', 'endDate'],
                'safe'
            ]
        ]);
    }

    public function getTimeZone(): ?string
    {
        if ($this->timeZoneOffset <> 0) {
            if ($this->timeZoneOffset < 0) {
                return gmdate('-H:i', 0 - $this->timeZoneOffset);
            } else {
                return gmdate('+H:i', $this->timeZoneOffset);
            }
        }
        return null;
    }

    public function getStartDateWithOffset(): \DateTimeInterface
    {
        $dateTime = empty($this->startDate)
            ? new \DateTimeImmutable(date('Y-01-01 00:00:00'))
            : new \DateTimeImmutable($this->startDate);

        if ($this->timeZoneOffset <> 0) {
            return $dateTime->modify((0 - $this->timeZoneOffset) . ' second');
        } else {
            return $dateTime;
        }
    }

    public function getEndDateWithOffset(): \DateTimeImmutable
    {
        $dateTime = empty($this->endDate)
            ? (new \DateTimeImmutable(date('Y-01-01 00:00:00')))->modify('+1 year')
            : new \DateTimeImmutable($this->endDate);

        if ($this->timeZoneOffset <> 0) {
            return $dateTime->modify((0 - $this->timeZoneOffset) . ' second');
        } else {
            return $dateTime;
        }
    }

    public function getGroupBy(): string
    {
        $startDate = empty($this->startDate)
            ? new \DateTimeImmutable(date('Y-01-01 00:00:00'))
            : new \DateTimeImmutable($this->startDate);

        $endDate = empty($this->endDate)
            ? (new \DateTimeImmutable(date('Y-01-01 00:00:00')))->modify('+1 year')
            : new \DateTimeImmutable($this->endDate);

        if ((int) $endDate->format('Y') - (int) $startDate->format('Y') > 1) {
            return static::GROUP_BY_YEARS;
        }

        if ((int) $endDate->format('Y') - (int) $startDate->format('Y') === 1) {
            return static::GROUP_BY_MONTHS;
        }

        if ((int) $endDate->format('m') - (int) $startDate->format('m') > 1) {
            return static::GROUP_BY_MONTHS;
        }

        if ((int) $endDate->format('m') - (int) $startDate->format('m') == 1) {
            return static::GROUP_BY_DAYS;
        }

        if ((int) $endDate->format('d') - (int) $startDate->format('d') > 1) {
            return static::GROUP_BY_DAYS;
        }

        return static::GROUP_BY_HOURS;
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), [
            'startDate', 'endDate', 'timeZoneOffset', 'startDateWithOffset', 'endDateWithOffset', 'groupBy'
        ]);
    }
}
