<?php

namespace app\modules\error\formatters;

class ExceptionJsonFormatter implements \JsonSerializable
{
    /**
     * @var \Exception|null
     */
    private $_exception;

    public function __construct(?\Exception $exception)
    {
        $this->_exception = $exception;
    }

    public function jsonSerialize()
    {
        return $this->_exception instanceof \Exception
            ? $this->getFormattedException($this->_exception)
            : null;
    }

    protected function getFormattedException(\Exception $exception): array
    {
        return YII_DEBUG
            ? [
                'type' => get_class($exception),
                'code' => $exception->getCode(),
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'message' => $exception->getMessage(),
                'trace' => $exception->getTraceAsString()
            ]
            : [
                'code' => $exception->getCode(),
                'message' => $exception->getMessage(),
            ];
    }
}