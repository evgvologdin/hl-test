<?php

namespace app\modules\error\controllers\actions;

use app\components\formatters\ErrorsResponseFormatter;
use app\modules\error\formatters\ExceptionJsonFormatter;
use Yii;
use yii\base\Action;

class ViewAction extends Action
{
    public function run()
    {
        $exception = Yii::$app->errorHandler->exception;

        return new ErrorsResponseFormatter(
            new ExceptionJsonFormatter($exception)
        );
    }
}
