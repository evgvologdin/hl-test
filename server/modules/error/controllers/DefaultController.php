<?php

namespace app\modules\error\controllers;


use app\components\web\Controller;

class DefaultController extends Controller
{

    public function actions()
    {
        return array_merge(parent::actions(), [
            'view' => actions\ViewAction::class
        ]);
    }
}
