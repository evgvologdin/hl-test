<?php

namespace app\modules\cashbox\models\mongo;

use yii\mongodb\ActiveQuery;

class CashboxQuery extends ActiveQuery
{
    public function getTimeZoneOffset(int $id): ?int
    {
        $result = $this->select(['timeZoneOffset'])->filterWhere(['_id' => $id])->asArray()->one();

        if ($result) {
            return (int) $result['timeZoneOffset'];
        } else {
            return null;
        }
    }
}
