<?php

namespace app\modules\cashbox\models\mongo;

use app\components\mongodb\ActiveRecord;

/**
 * Class Cashbox
 * @package app\modules\cashbox\models
 * @property integer $timeZoneOffset
 */
class Cashbox extends ActiveRecord
{
    public static function collectionName()
    {
        return ['hlebnitca', 'cashbox'];
    }

    public function attributes()
    {
        return array_merge(parent::attributes(), ['timeZoneOffset']);
    }

    public function rules()
    {
        return [
            [['idx', 'timeZoneOffset'], 'safe']
        ];
    }

    public static function find()
    {
        return new CashboxQuery(get_called_class());
    }
}
