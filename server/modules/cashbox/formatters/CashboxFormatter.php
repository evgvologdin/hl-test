<?php

namespace app\modules\cashbox\formatters;

use app\modules\cashbox\models\mongo\Cashbox;

class CashboxFormatter implements \JsonSerializable
{
    /**
     * @var Cashbox
     */
    private $_model;

    public function __construct(Cashbox $model)
    {
        $this->_model = $model;
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->_model->id,
            'name' => "Касса {$this->_model->id}"
        ];
    }
}
