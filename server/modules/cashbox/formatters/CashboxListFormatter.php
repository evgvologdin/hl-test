<?php

namespace app\modules\cashbox\formatters;

use app\modules\cashbox\models\mongo\Cashbox;

class CashboxListFormatter implements \JsonSerializable
{
    /**
     * @var Cashbox[]
     */
    private $_models = [];

    public function __construct(array $models)
    {
        $this->_models = $models;
    }

    public function jsonSerialize()
    {
        return array_map(function (Cashbox $model) {
            return new CashboxFormatter($model);
        }, $this->_models);
    }
}
