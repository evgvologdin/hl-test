<?php

namespace app\modules\cashbox\controllers;

use app\components\web\Controller;
use app\modules\cashbox\actions\ViewAction;

class DefaultController extends Controller
{
    public function actions()
    {
        return array_merge(parent::actions(), [
            'view' => actions\ViewAction::class
        ]);
    }
}
