<?php

namespace app\modules\cashbox\controllers\actions;

use app\modules\cashbox\formatters\CashboxListFormatter;
use app\modules\cashbox\models\mongo\Cashbox;
use app\components\formatters\SuccessResponseFormatter;
use yii\base\Action;

class ViewAction extends Action
{
    public function run()
    {
        $models = Cashbox::find()->all();

        return new SuccessResponseFormatter(
            new CashboxListFormatter($models)
        );
    }
}
