<?php

namespace app\commands;

use app\modules\cashbox\models\mongo\Cashbox;
use app\modules\check\models\async\Create;
use app\modules\check\models\mongo\Check;
use yii\console\Controller;

class FillTestDataController extends Controller
{
    public function actionIndex()
    {
        $this->createCashboxes();
        $this->createChecks();
    }

    protected function createCashboxes()
    {
        $count = Cashbox::find()->count();

        if (!!$count) {
            return;
        }

        $this->createCashbox(-8);
        $this->createCashbox(0);
        $this->createCashbox(8);
    }

    protected function createCashbox(int $timeZoneOffset = 0)
    {
        $model = new Cashbox();
        $model->timeZoneOffset = $timeZoneOffset * 60 * 60;
        $model->save();
    }

    protected function createChecks()
    {
        $count = Check::find()->count();

        if (!!$count) {
            return;
        }

        $cashboxes = Cashbox::find()->all();
        $startDate = new \DateTimeImmutable('2019-01-01 00:00:00');

        while ($startDate->format('Y') == '2019') {
            echo 'create checks date: ' . $startDate->format('d.m.Y') . PHP_EOL;
            $this->fillCashbox($cashboxes, $startDate);
            $startDate = $startDate->modify('+1 day');
        }
    }

    protected function fillCashbox(array $cashboxes, \DateTimeInterface $date)
    {
        foreach ($cashboxes as $cashbox) {

            $count = mt_rand(400, 800);

            for ($i = 0; $i <= $count; $i++) {
                $data = [
                    'cashboxId' => $cashbox->_id,
                    'dateTime' => $date->modify(mt_rand(6, 21) . ' hours')->modify(mt_rand(0, 59) . ' minutes')->format('Y-m-d H:i:s'),
                    'totalSum' => mt_rand(800, 1500) / 10
                ];

                $model = new Create();
                $model->load($data, '');
                $model->save(false);
            }

            echo "created checks for cashbox {$cashbox->_id}: {$count}\n";
        }
    }

}
