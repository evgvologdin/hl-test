var updateData = function (query) {

    $('#root').html($('<center><div class="spinner-border" role="status"><span class="sr-only">Loading...</span></div></center>'));

    var result = $('<table class="table" />');

    $.get({
        url: "http://localhost:10000/cashbox",
        contentType: "application/json",
        success: function (response) {

            var cashboxes = response.response;
            var heading = $('<tr><th /></tr>');

            for (var i in cashboxes) {
                heading.append($('<th>' + cashboxes[i].name + '</th>'));
            }

            result.append(heading);

            $.post({
                url: "http://localhost:10000/statistics/summary",
                contentType: "application/json",
                data: !!query ? JSON.stringify(query) : null,
                success: function (response) {
                    for (var i in response.response) {

                        (function (item) {
                            var row = $('<tr />');
                            var name = $('<a href="#">' + item.name + '</a>').click(function (e) {
                                e.preventDefault();
                                updateData(item.query);
                            });
                            row.append($('<td />').html(name));

                            for (var i in cashboxes) {
                                var value = '-';

                                for (var j in item.cashboxes) {
                                    if (item.cashboxes[j].cashboxId == cashboxes[i].id) {

                                        value = Number(item.cashboxes[j].totalSum).toLocaleString("ru");
                                    }
                                }

                                row.append($('<td>' + value + '</td>'));
                            }

                            result.append(row);
                        })(response.response[i]);
                    }

                    $('#root').html(result);
                }
            });
        }
    });
}

updateData(null);
